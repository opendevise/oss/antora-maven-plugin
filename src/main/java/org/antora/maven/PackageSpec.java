package org.antora.maven;

import java.util.Objects;

public class PackageSpec {
    private final String name;

    private final String versionSpec;

    public PackageSpec(String name) {
        this(name, null);
    }

    public PackageSpec(String name, String versionSpec) {
        if (name == null) throw new IllegalArgumentException("name cannot be null");
        if (name.isEmpty()) throw new IllegalArgumentException("name cannot be empty");
        this.name = name;
        this.versionSpec = versionSpec;
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        if (this.versionSpec == null || "latest".equals(this.versionSpec)) return this.name;
        return this.name + "@" + this.versionSpec;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        return this.name.equals(((PackageSpec) other).name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name);
    }

    public static PackageSpec valueOf(String packageSpec) {
        if (packageSpec.indexOf("@", 1) < 0) return new PackageSpec(packageSpec);
        String[] nameAndVersionSpec = packageSpec.split("(?!^)@", 2);
        return new PackageSpec(nameAndVersionSpec[0], nameAndVersionSpec[1]);
    }
}
