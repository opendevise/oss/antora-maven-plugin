package org.antora.maven;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.nio.file.Files.createSymbolicLink;
import static java.nio.file.Files.readSymbolicLink;

public class SystemNodeLinker {
    private static final String RESOLVE_NODE_HOME_SCRIPT = "const p=require('path');" +
        "function hasNpm(dir){try{require.resolve(p.join(dir,'npm/bin/npm-cli.js'));return dir}catch{}};" +
        "const exe=process.execPath,root=p.join(exe,p.basename(p.dirname(exe))==='bin'?'../..':'..');" +
        "const nm='node_modules',pkgs=hasNpm(p.join(root,'lib',nm))||hasNpm(p.join(root,nm))||'';" +
        "pkgs&&exe+os.EOL+pkgs";

    private final Log log;

    private final Path symbolicNodeHomeDir;

    public SystemNodeLinker(Log log, File symbolicNodeHomeDir) {
        this.log = log;
        this.symbolicNodeHomeDir = symbolicNodeHomeDir.toPath();
    }

    public void linkNode(String nodeExecutable) throws MojoExecutionException {
        List<String> targets = null;
        String quotedNodeExecutable = "nodeExecutable \"" + nodeExecutable + "\"";
        try {
            if ((targets = resolveSystemNodeHome(nodeExecutable)).isEmpty()) targets = null;
        } catch (IOException ioe) {
            String msg = "Cannot run " + quotedNodeExecutable;
            Throwable cause = ioe.getCause() == null ? ioe : ioe.getCause();
            log.error(msg + ": " + cause.getMessage());
            throw new MojoExecutionException(msg, cause);
        }
        if (targets == null) {
            String msg = "Cannot verify compatible Node.js installation for " + quotedNodeExecutable;
            log.error(msg);
            throw new MojoExecutionException(msg);
        }
        Path nodeExecutableTarget = Path.of(targets.get(0));
        Path nodeModulesDirectoryTarget = Path.of(targets.get(1));
        Path nodeExecutableLink = this.symbolicNodeHomeDir.resolve("node" + getFileExtension(nodeExecutableTarget));
        Path nodeModulesDirectoryLink = this.symbolicNodeHomeDir.resolve("node_modules");
        if (isSymbolicLinkTo(nodeExecutableLink, nodeExecutableTarget) &&
            isSymbolicLinkTo(nodeModulesDirectoryLink, nodeModulesDirectoryTarget)) {
            log.info("System Node.js already linked for " + quotedNodeExecutable);
            return;
        }
        try {
            FileUtils.emptyDirectory(this.symbolicNodeHomeDir.toFile());
            createSymbolicLink(nodeExecutableLink, nodeExecutableTarget);
            createSymbolicLink(nodeModulesDirectoryLink, nodeModulesDirectoryTarget);
            log.info("Linking system Node.js for " + quotedNodeExecutable);
        } catch (IOException ioe) {
            String msg = "Unable to link system Node.js for " + quotedNodeExecutable;
            log.error(msg);
            throw new MojoExecutionException(msg, ioe);
        }
    }

    public void unlinkNode() {
        if (!this.symbolicNodeHomeDir.toFile().isDirectory()) return;
        List.of("node", "node.exe", "node_modules").forEach(name -> {
            Path path = this.symbolicNodeHomeDir.resolve(name);
            if (isSymbolicLink(path)) path.toFile().delete();
        });
    }

    private List<String> resolveSystemNodeHome(String nodeExecutable) throws IOException {
        Process p = new ProcessBuilder(nodeExecutable, "-p", RESOLVE_NODE_HOME_SCRIPT).start();
        try (BufferedReader stdoutReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stderrReader = new BufferedReader(new InputStreamReader(p.getErrorStream()))) {
            List<String> output = stdoutReader.lines().collect(Collectors.toList());
            if (output.size() == 1 && output.get(0).isEmpty()) output.clear();
            int exitValue;
            try {
                p.waitFor(1, TimeUnit.SECONDS);
                exitValue = p.exitValue();
            } catch (InterruptedException ie) {
                exitValue = 1;
            }
            if (exitValue == 0 && (output.size() == 2 || output.isEmpty())) return output;
            String msg = "Not a valid Node.js executable";
            IOException cause = null;
            if (exitValue > 0) {
                List<String> errLines = stderrReader.lines().collect(Collectors.toList());
                if (!errLines.isEmpty() &&
                    !Pattern.compile(" (?:flag|option)[: ].*[-']?p").matcher(errLines.get(0)).find()) {
                    cause = new IOException("error=" + exitValue + ", " + String.join("\n", errLines));
                }
            }
            throw new IOException(msg, cause);
        }
    }

    private String getFileExtension(Path path) {
        String fileName = path.getFileName().toString();
        int fileExtensionIdx = fileName.lastIndexOf('.');
        if (fileExtensionIdx < 0) return "";
        return fileName.substring(fileExtensionIdx);
    }

    private boolean isSymbolicLink(Path candidate) {
        try {
            readSymbolicLink(candidate);
            return true;
        } catch (IOException ioe) {
            return false;
        }
    }

    private boolean isSymbolicLinkTo(Path candidate, Path target) {
        try {
            return readSymbolicLink(candidate).equals(target);
        } catch (IOException ioe) {
            return false;
        }
    }
}
