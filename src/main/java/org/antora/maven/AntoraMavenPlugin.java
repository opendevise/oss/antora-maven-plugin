package org.antora.maven;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.maven.execution.MavenExecutionRequest;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.twdata.maven.mojoexecutor.MojoExecutor.Element;
import static org.twdata.maven.mojoexecutor.MojoExecutor.element;

/**
 * Sets up and runs Antora on your Maven project using the specified playbook file or playbook provider.
 */
@Mojo(name = "antora", requiresProject = false)
public class AntoraMavenPlugin extends AbstractMojo {
    private static final String ANTORA_PACKAGE_NAME = "antora";

    private static final String ANTORA_CLI_PACKAGE_NAME = "@antora/cli";

    private static final String ANTORA_COMMAND_NAME = "antora";

    private static final String PACKAGE_FILE = "package.json";

    private static final String PACKAGE_LOCK_FILE = "package-lock.json";

    /**
     * The project currently being build.
     */
    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject mavenProject;

    /**
     * The current Maven session.
     */
    @Parameter(defaultValue = "${session}", readonly = true)
    private MavenSession mavenSession;

    private final Platform platform = new Platform();

    /**
     * The Maven BuildPluginManager component.
     */
    @Component
    private BuildPluginManager pluginManager;

    /**
     * Additional environment variables to set when invoking Antora.
     */
    @Parameter(alias = "env")
    private Map<String, String> environmentVariables = Collections.emptyMap();

    /**
     * Sets the name or path of the Node.js executable to use. Implicitly selects which Node.js installation to use to
     * invoke npm and npx. When specified, Node.js is not installed locally and the nodeVersion parameter is ignored.
     */
    @Parameter(property = "node.executable")
    private String nodeExecutable;

    /**
     * The location where Node.js is installed or linked. A node directory will be created inside this directory that
     * hosts the installation files or symlinks.
     */
    @Parameter(property = "node.installDirectory")
    private File nodeInstallDirectory;

    /**
     * The version of Node.js to install. Can be an exact or fuzzy version number with an optional leading "v". Also
     * accepts the keyword "lts", which resolves to the latest LTS release, and "latest", which resolves to the latest
     * available release.
     */
    @Parameter(defaultValue = "lts", property = "node.version")
    private String nodeVersion;

    /**
     * Sets the list of CLI options to pass to Antora. If the option is a flag, the form should be either
     * name?=true|false or name (which implies name?=true). Otherwise, the form should be name=value. In either case,
     * the equals sign can be replaced with a space. The value should never be enclosed in quotes.
     */
    @Parameter
    private List<String> options = Collections.emptyList();

    /**
     * Sets the list of additional CLI options to pass to Antora. This parameter accepts the same values as the options
     * parameter. The intended use of this parameter is to add additional options in a non-default execution or from the
     * command line without having to redefine all the options again.
     */
    @Parameter(property = "antora.additionalOptions")
    private List<String> additionalOptions = Collections.emptyList();

    /**
     * Sets the list of npm packages to install (in addition to the Antora package) when installing Antora. The form
     * should be the same as used with npm install (e.g., @asciidoctor/pdf or @asciidoctor/pdf@latest). The antora
     * package should not be included in this list. Not used when package.json is present and declares dependencies or
     * the playbookProvider parameter is specified.
     */
    @Parameter
    private List<String> packages = Collections.emptyList();

    /**
     * Sets the list of additional npm packages to install when installing Antora. This parameter accepts the same
     * values as the packages parameter. The intended use of this parameter is to add additional packages in a
     * non-default execution or from the command line without having to declare all the packages again. Not used when
     * package.json is present and declares dependencies.
     */
    @Parameter(property = "antora.additionalPackages")
    private List<String> additionalPackages = Collections.emptyList();

    /**
     * Sets the Antora playbook file to pass to Antora.
     */
    @Parameter(defaultValue = "antora-playbook.yml", property = "antora.playbook", required = true)
    private File playbook;

    /**
     * Sets the location of a playbook template to sideload or download to provided-antora-playbook.yml to use as the
     * Antora playbook. When set, the playbook parameter is ignored.
     */
    @Parameter
    private PlaybookProvider playbookProvider;

    /**
     * Specifies whether the Antora site generation should be skipped.
     */
    @Parameter(defaultValue = "false", property = "antora.skip")
    private boolean skip;

    /**
     * Sets the version of Antora to use.
     */
    @Parameter(defaultValue = "latest")
    private String version;

    public void execute() throws MojoExecutionException, MojoFailureException {
        if (skip) {
            getLog().info("Skipping Antora site generation per configuration.");
            return;
        }
        validate();
        boolean standalone = !this.mavenSession.getRequest().isProjectPresent();
        File basedir = standalone ? Path.of("").toAbsolutePath().toFile() : this.mavenProject.getBasedir();
        if (this.nodeInstallDirectory == null) {
            this.nodeInstallDirectory = standalone ? basedir : new File(this.mavenProject.getBuild().getDirectory());
        }
        File nodeHomeDir = new File(this.nodeInstallDirectory, "node");
        File npmCacheDir = null;
        FrontendMojoExecutor frontendMojoExecutor = new FrontendMojoExecutor(this.pluginManager, this.mavenSession,
            element("installDirectory", this.nodeInstallDirectory.getPath()));
        SystemNodeLinker systemNodeLinker = new SystemNodeLinker(getLog(), nodeHomeDir);
        if (this.nodeExecutable == null) {
            systemNodeLinker.unlinkNode();
            frontendMojoExecutor.executeMojo("install-node-and-npm",
                element("nodeVersion", new NodeVersionResolver(getLog()).resolveVersion(this.nodeVersion)));
            npmCacheDir = new File(nodeHomeDir, "_npm");
        } else {
            systemNodeLinker.linkNode(Path.of(this.nodeExecutable).toString());
        }
        Map<String, PackageSpec> allPackages = new LinkedHashMap<>();
        String playbookArgument;
        boolean usingNodeModules = false;
        if (this.playbookProvider == null) {
            playbookArgument = basedir.toPath().relativize(this.playbook.toPath()).toString();
            if (packageFileExistsWithDependencies(new File(basedir, PACKAGE_FILE))) {
                usingNodeModules = true;
                boolean hasPackageLockFile = new File(basedir, PACKAGE_LOCK_FILE).exists();
                frontendMojoExecutor.executeMojo("npm", element("arguments", hasPackageLockFile ? "ci" : "i"),
                    element("environmentVariables", npmInstallEnvironmentVariables(hasPackageLockFile, npmCacheDir)));
                String antoraCommandName = this.platform.npmPackageCommandName(ANTORA_COMMAND_NAME);
                if (!Path.of(basedir.getPath(), "node_modules", ".bin", antoraCommandName).toFile().exists()) {
                    addPackage(antoraCliPackage(this.version), allPackages);
                }
            } else {
                addPackage(antoraPackage(this.version), allPackages);
                addPackages(this.packages, allPackages);
                addPackages(this.additionalPackages, allPackages);
            }
        } else {
            File providedPlaybook = new File(basedir, "provided-antora-playbook.yml");
            playbookArgument = providedPlaybook.getName();
            Map<String, PackageSpec> providedPackages = new ProvidedPlaybookRetriever(getLog(), basedir)
                .retrievePlaybook(this.playbookProvider, providedPlaybook);
            PackageSpec antoraPackage = providedPackages.remove(ANTORA_PACKAGE_NAME);
            providedPackages.remove(ANTORA_CLI_PACKAGE_NAME);
            addPackage(antoraPackage == null ? antoraPackage(this.version) : antoraPackage, allPackages);
            allPackages.putAll(providedPackages);
            addPackages(this.additionalPackages, allPackages);
        }
        if (!usingNodeModules) removeNodeModules(basedir);
        List<String> allOptions = collectOptions(this.options, this.additionalOptions);
        try {
            frontendMojoExecutor.executeMojo("npx",
                element("arguments", npxAntoraArguments(allPackages.values(), allOptions, playbookArgument)),
                element("environmentVariables", antoraEnvironmentVariables(this.environmentVariables, npmCacheDir)));
        } catch (MojoExecutionException e) {
            throw new MojoFailureException("Antora failed to generate site successfully", e);
        }
    }

    private void validate() throws MojoExecutionException {
        File homeNodeModulesDir = new File(System.getProperty("user.home"), "node_modules");
        if (homeNodeModulesDir.isDirectory() && !FileUtils.isEmptyDirectory(homeNodeModulesDir)) {
            getLog().warn(
                "Detected the existence of $HOME/node_modules, which is not compatible with this plugin. Please remove it.");
        }
        this.playbookProvider = PlaybookProvider.validate(this.playbookProvider);
        if (this.playbookProvider == null && !this.playbook.exists()) {
            String message = "Antora playbook file not found: " + this.playbook;
            getLog().error("Cannot run Antora because " + message);
            throw new MojoExecutionException(message);
        }
    }

    private Element[] npmInstallEnvironmentVariables(boolean hasPackageLockFile, File npmCacheDir) {
        Map<String, String> env = new HashMap<>();
        if (npmCacheDir != null) env.put("npm_config_cache", npmCacheDir.getPath());
        env.put("npm_config_fund", "false");
        env.put("npm_config_omit", "optional");
        if (!hasPackageLockFile) env.put("npm_config_package_lock", "false");
        env.put("npm_config_update_notifier", "false");
        return mapToElementArray(env);
    }

    private String npxAntoraArguments(Collection<PackageSpec> allPackages, List<String> allOptions, String playbook) {
        List<String> arguments = new ArrayList<>(List.of("--yes"));
        if (allPackages.isEmpty()) {
            arguments.set(0, "--offline");
        } else {
            for (PackageSpec packageSpec : allPackages) {
                drain(arguments.add("--package") && arguments.add(packageSpec.toString()));
            }
        }
        arguments.add(ANTORA_COMMAND_NAME);
        arguments.addAll(antoraOptions(allOptions));
        arguments.add(playbook);
        return String.join(" ", arguments);
    }

    private List<String> antoraOptions(List<String> allOptions) {
        Map<String, List<String>> optionsMap = new LinkedHashMap<>();
        MavenExecutionRequest mavenExecutionRequest = this.mavenSession.getRequest();
        if (mavenExecutionRequest.isShowErrors()) optionsMap.put("stacktrace", null);
        if (mavenExecutionRequest.isProjectPresent()) {
            Path relativeBuildDirectory = this.mavenProject.getBasedir()
                .toPath()
                .relativize(Path.of(this.mavenProject.getBuild().getDirectory(), "site"));
            optionsMap.put("to-dir", List.of(this.platform.escapeCommandLineArgument(relativeBuildDirectory)));
        }
        if (allOptions != null) appendOptions(allOptions, optionsMap);
        List<String> opts = new ArrayList<>();
        optionsMap.forEach((name, values) -> {
            String flag = "--" + name;
            if (values == null) {
                opts.add(flag);
                return;
            }
            values.forEach(value -> drain(opts.add(flag) && opts.add(value)));
        });
        return opts;
    }

    private Element[] antoraEnvironmentVariables(Map<String, String> env, File npmCacheDir) {
        env = env.isEmpty() ? new HashMap<>() : new HashMap<>(env);
        String isTTY = System.getenv("IS_TTY");
        if (isTTY == null) env.put("IS_TTY", (isTTY = "true"));
        if ("true".equals(isTTY) && System.getenv("FORCE_COLOR") == null &&
            this.mavenSession.getRequest().isInteractiveMode()) {
            env.put("FORCE_COLOR", "true");
        }
        String nodeOptions = env.containsKey("NODE_OPTIONS") ? env.get("NODE_OPTIONS") : System.getenv("NODE_OPTIONS");
        // add --no-global-search-paths to attempt to isolate execution from system
        env.put("NODE_OPTIONS", (nodeOptions == null ? "" : nodeOptions + " ") + "--no-global-search-paths");
        if (npmCacheDir != null) env.put("npm_config_cache", npmCacheDir.getPath());
        env.put("npm_config_fund", "false");
        env.put("npm_config_lockfile_version", "3");
        env.put("npm_config_omit", "optional");
        env.put("npm_config_update_notifier", "false");
        return mapToElementArray(env);
    }

    private PackageSpec antoraPackage(String versionSpec) {
        return new PackageSpec(ANTORA_PACKAGE_NAME, versionSpec);
    }

    private PackageSpec antoraCliPackage(String versionSpec) {
        return new PackageSpec(ANTORA_CLI_PACKAGE_NAME, versionSpec);
    }

    private void addPackage(PackageSpec value, Map<String, PackageSpec> to) {
        to.put(value.getName(), value);
    }

    private void addPackages(List<String> packageSpecs, Map<String, PackageSpec> to) {
        if (packageSpecs.isEmpty()) return;
        for (String packageSpec : packageSpecs) {
            if (packageSpec == null || (packageSpec = packageSpec.trim()).isEmpty()) continue;
            PackageSpec value = PackageSpec.valueOf(packageSpec);
            String name = value.getName();
            if (!ANTORA_PACKAGE_NAME.equals(name) && !ANTORA_CLI_PACKAGE_NAME.equals(name)) to.put(name, value);
        }
    }

    private boolean packageFileExistsWithDependencies(File packageFile) {
        if (!packageFile.exists()) return false;
        try (FileReader reader = new FileReader(packageFile)) {
            JsonObject pkg = JsonParser.parseReader(reader).getAsJsonObject();
            return (pkg.has("dependencies") && !pkg.getAsJsonObject("dependencies").isEmpty()) ||
                (pkg.has("devDependencies") && !pkg.getAsJsonObject("devDependencies").isEmpty());
        } catch (IOException ex) {
            return true;
        }
    }

    private void removeNodeModules(File basedir) {
        try {
            if (FileUtils.removeDirectory(new File(basedir, "node_modules"))) {
                getLog().info("Removing node_modules in " + basedir);
            }
        } catch (IOException e) {
            getLog().warn("Detected the existence of node_modules in " + basedir +
                ", which is not compatible with the current configuration of this plugin. Please remove it.");
        }
    }

    private Element[] mapToElementArray(Map<String, String> map) {
        return map.entrySet()
            .stream()
            .map(envEntry -> element(envEntry.getKey(), envEntry.getValue()))
            .toArray(Element[]::new);
    }

    private void appendOptions(List<String> from, Map<String, List<String>> to) {
        from.forEach((text) -> {
            if (text == null || (text = text.stripLeading()).isEmpty()) return;
            String[] segments = text.split("[ =]", 2);
            String name = segments[0];
            if (name.startsWith("--")) name = name.substring(2);
            int indexOfLastCharInName = name.length() - 1;
            char lastCharInName = name.charAt(indexOfLastCharInName);
            if (lastCharInName == '!') {
                to.remove(name.substring(0, indexOfLastCharInName));
                return;
            }
            String value = segments.length == 2 ? segments[1].stripTrailing() : null;
            if (lastCharInName == '?') {
                name = name.substring(0, indexOfLastCharInName);
                if (value == null || "false".equals(value)) {
                    value = null;
                } else if (value.contains(":")) {
                    String[] valueAndFallbackValue = value.split(":", 2);
                    if ((value = valueAndFallbackValue[0].stripLeading()).isEmpty() &&
                        ((value = valueAndFallbackValue[1]).isEmpty() || "false".equals(value))) {
                        value = null;
                    }
                }
                drain(value == null ? to.remove(name) : to.put(name, null));
            } else if (value == null) {
                to.put(name, null);
            } else {
                value = this.platform.escapeCommandLineArgument(value);
                if (lastCharInName == ']' && indexOfLastCharInName > 1 && name.endsWith("[]")) {
                    List<String> instances = to.get((name = name.substring(0, indexOfLastCharInName - 1)));
                    drain(instances == null ? to.put(name, createSingleValueList(value)) : instances.add(value));
                } else {
                    to.put(name, createSingleValueList(value));
                }
            }
        });
    }

    private List<String> collectOptions(List<String> main, List<String> additional) {
        List<String> result = new ArrayList<>();
        if (!main.isEmpty()) result.addAll(main);
        if (!additional.isEmpty()) result.addAll(additional);
        return result.isEmpty() ? null : result;
    }

    private List<String> createSingleValueList(String value) {
        List<String> list = new ArrayList<>(1);
        list.add(value);
        return list;
    }

    private void drain(Object result) {}
}
