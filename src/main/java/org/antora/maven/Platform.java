package org.antora.maven;

import java.nio.file.Path;
import java.util.regex.Pattern;

public class Platform {
    private static final Pattern FRAGMENTED_COMMAND_LINE_ARGUMENT_RX = Pattern.compile("[ '\"]");

    private final boolean windows;

    public Platform() {
        this.windows = System.getProperty("os.name").toLowerCase().contains("windows");
    }

    public String npmPackageCommandName(String name) {
        return this.windows ? name + ".cmd" : name;
    }

    public String escapeCommandLineArgument(Path value) {
        return escapeCommandLineArgument(value.toString());
    }

    public String escapeCommandLineArgument(String value) {
        if (value.isEmpty()) return "\"\"";
        if (!FRAGMENTED_COMMAND_LINE_ARGUMENT_RX.matcher(value).find()) return value;
        String q = "\"";
        if (value.contains(q)) {
            if (value.contains((q = "'"))) {
                // NOTE frontend plugin doesn't support escaped quotes; revert to character reference
                value = value.replace((q = "\""), "&quot;");
            } else if (this.windows) {
                value = value.replace("\"", "\"\"");
            }
        }
        return q + value + q;
    }
}
