package org.antora.maven;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlaybookProvider {
    private static final Map<String, String> DOWNLOAD_URL_TEMPLATES =
        new HashMap<>(Map.of("github.com", "https://raw.githubusercontent.com/%s/%s/%s", "gitlab.com",
            "https://gitlab.com/%s/-/raw/%s/%s", "bitbucket.org", "https://bitbucket.org/%s/raw/%s/%s"));

    private static final Map<String, String> HOST_ALIASES =
        new HashMap<>(Map.of("github", "github.com", "gitlab", "gitlab.com", "bitbucket", "bitbucket.org"));

    private static final List<String> TRY_LOCAL_BRANCH_VALUES = List.of("false", "first", "only", "never");

    private String host;

    private String repository;

    private String branch;

    private String path;

    private String tryLocalBranch;

    public PlaybookProvider() {
        this.host = "github.com";
        this.repository = null;
        this.branch = "docs-build";
        this.path = "antora-playbook-template.yml";
        this.tryLocalBranch = null;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = HOST_ALIASES.getOrDefault(host, host);
    }

    public String getRepository() {
        return this.host == null ? null : this.repository;
    }

    public void setRepository(String repository) {
        if (repository != null && repository.indexOf(':') > 0) {
            String[] hostAndRepository = repository.split(":", 2);
            setHost(hostAndRepository[0]);
            this.repository = hostAndRepository[1];
        } else {
            this.repository = repository;
        }
    }

    public String getBranch() {
        return this.branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        if (path != null && path.indexOf(':') > 0) {
            String[] branchAndPath = path.split(":", 2);
            setBranch(branchAndPath[0]);
            this.path = branchAndPath[1];
        } else {
            this.path = path;
        }
    }

    public String getTryLocalBranch() {
        return this.tryLocalBranch;
    }

    public void setTryLocalBranch(String tryLocalBranch) {
        if (tryLocalBranch == null) {
            this.tryLocalBranch = null;
        } else if (TRY_LOCAL_BRANCH_VALUES.contains(tryLocalBranch)) {
            this.tryLocalBranch = "false".equals(tryLocalBranch) ? "never" : tryLocalBranch;
        } else {
            this.tryLocalBranch = "first";
        }
    }

    public boolean isDownloadAvailable() {
        return !"only".equals(getTryLocalBranch()) && getRepository() != null &&
            DOWNLOAD_URL_TEMPLATES.containsKey(getHost());
    }

    public boolean isSideloadAvailable() {
        return !"never".equals(getTryLocalBranch()) && (getRepository() == null || getTryLocalBranch() != null);
    }

    public URL getDownloadURL() throws MalformedURLException {
        return new URL(String.format(DOWNLOAD_URL_TEMPLATES.get(getHost()), getRepository(), getBranch(), getPath()));
    }

    public String getRevPath() {
        return getBranch() + ':' + getPath();
    }

    public static PlaybookProvider validate(PlaybookProvider candidate) {
        if (candidate == null || candidate.getBranch() == null || candidate.getPath() == null) return null;
        if (candidate.isSideloadAvailable() || candidate.isDownloadAvailable()) return candidate;
        return null;
    }
}
