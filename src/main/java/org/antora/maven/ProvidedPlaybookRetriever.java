package org.antora.maven;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ProvidedPlaybookRetriever {
    private static final String PACKAGES_MAGIC_COMMENT_PREFIX = "# PACKAGES ";

    private final Log log;

    private final File basedir;

    public ProvidedPlaybookRetriever(Log log, File basedir) {
        this.log = log;
        this.basedir = basedir;
    }

    public Map<String, PackageSpec> retrievePlaybook(PlaybookProvider provider, File playbook)
        throws MojoExecutionException {
        if (playbook.exists()) {
            log.info("Provided playbook has already been retrieved.");
            log.info("Remove " + playbook.getName() + " to retrieve a new copy.");
        } else {
            log.info("Retrieving Antora playbook from provider.");
            boolean tryDownload = provider.isDownloadAvailable();
            if (provider.isSideloadAvailable()) {
                try {
                    new FileSideloader(this.basedir).sideload(provider.getRevPath(), playbook);
                    tryDownload = false;
                } catch (IOException ioe) {
                    playbook.delete();
                    if (!tryDownload) {
                        String msg =
                            "Unable to find provided playbook in local repository and no remote repository is specified from which to download it";
                        log.error(msg);
                        throw new MojoExecutionException(msg, ioe);
                    }
                }
            }
            if (tryDownload) {
                try {
                    new FileDownloader().download(provider.getDownloadURL(), playbook);
                } catch (IOException ioe) {
                    playbook.delete();
                    String msg = "Unable to retrieve provided playbook from remote repository.";
                    log.error(msg);
                    throw new MojoExecutionException(msg, ioe);
                }
            }
        }
        return extractPackages(playbook);
    }

    private Map<String, PackageSpec> extractPackages(File playbook) {
        Map<String, PackageSpec> packages = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(playbook))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (!line.startsWith(PACKAGES_MAGIC_COMMENT_PREFIX)) continue;
                String[] specs = line.stripTrailing().substring(PACKAGES_MAGIC_COMMENT_PREFIX.length()).split(" +");
                for (String spec : specs) {
                    PackageSpec pkg = PackageSpec.valueOf(spec);
                    packages.put(pkg.getName(), pkg);
                }
                break;
            }
        } catch (IOException ex) {}
        return packages;
    }
}
