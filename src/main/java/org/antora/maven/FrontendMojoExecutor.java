package org.antora.maven;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.PluginManagement;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.twdata.maven.mojoexecutor.MojoExecutor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FrontendMojoExecutor {
    private static final String FRONTEND_MAVEN_PLUGIN_COORDINATES = "com.github.eirslett:frontend-maven-plugin:1.15.0";

    private final List<MojoExecutor.Element> baseConfigurationElements;

    private final MojoExecutor.ExecutionEnvironment executionEnvironment;

    private final Plugin plugin;

    public FrontendMojoExecutor(BuildPluginManager pluginManager, MavenSession mavenSession,
        MojoExecutor.Element... configuration) {
        this.baseConfigurationElements = new ArrayList<>(Arrays.asList(configuration));
        this.plugin = getFrontendMavenPlugin(mavenSession);
        this.executionEnvironment = new MojoExecutor.ExecutionEnvironment(mavenSession, pluginManager);
    }

    public void executeMojo(String goal, MojoExecutor.Element... configuration) throws MojoExecutionException {
        List<MojoExecutor.Element> mergedConfiguration = new ArrayList<>(Arrays.asList(configuration));
        mergedConfiguration.addAll(this.baseConfigurationElements);
        MojoExecutor.executeMojo(this.plugin, MojoExecutor.goal(goal),
            MojoExecutor.configuration(mergedConfiguration.toArray(new MojoExecutor.Element[0])),
            this.executionEnvironment);
    }

    private Plugin getFrontendMavenPlugin(MavenSession mavenSession) {
        String[] coordinates = FRONTEND_MAVEN_PLUGIN_COORDINATES.split(":", 3);
        Plugin result = MojoExecutor.plugin(MojoExecutor.groupId(coordinates[0]),
            MojoExecutor.artifactId(coordinates[1]), MojoExecutor.version(coordinates[2]));
        PluginManagement pluginManagement = mavenSession.getCurrentProject().getPluginManagement();
        if (pluginManagement != null) {
            for (Plugin candidate : pluginManagement.getPlugins()) {
                if (candidate.getGroupId().equals(result.getGroupId()) &&
                    candidate.getArtifactId().equals(candidate.getArtifactId())) {
                    result.setVersion(candidate.getVersion());
                    break;
                }
            }
        }
        return result;
    }
}
