package org.antora.maven;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.stream.Stream;

public class FileUtils {
    public static void emptyDirectory(File directory) throws IOException {
        if (!directory.isDirectory()) {
            Files.deleteIfExists(directory.toPath());
            directory.mkdirs();
            return;
        }
        try (Stream<Path> walk = Files.walk(directory.toPath()).sorted(Comparator.reverseOrder())) {
            walk.map(Path::toFile).filter(file -> !file.equals(directory)).forEach(File::delete);
        }
    }

    public static boolean isEmptyDirectory(File directory) {
        if (!directory.isDirectory()) return false;
        try (Stream<Path> entries = Files.list(directory.toPath())) {
            return entries.findFirst().isEmpty();
        } catch (IOException ioe) {
            return false;
        }
    }

    public static boolean removeDirectory(File directory) throws IOException {
        if (!directory.isDirectory()) return Files.deleteIfExists(directory.toPath());
        try (Stream<Path> walk = Files.walk(directory.toPath()).sorted(Comparator.reverseOrder())) {
            walk.map(Path::toFile).forEach(File::delete);
        }
        return true;
    }
}
