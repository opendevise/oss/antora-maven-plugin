package org.antora.maven;

import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;

public class FileSideloader {
    private File gitdir;

    public FileSideloader(File dir) throws IOException {
        this.gitdir = resolveGitDir(dir);
    }

    public void sideload(String revPath, File toFile) throws IOException {
        try (OutputStream os = new FileOutputStream(toFile);
            Repository repo = new RepositoryBuilder().setGitDir(this.gitdir).build()) {
            ObjectId resolvedObjectId = repo.resolve(revPath);
            if (resolvedObjectId == null) throw new IOException("revpath not found in local repository: " + revPath);
            repo.newObjectReader().open(resolvedObjectId).copyTo(os);
        }
    }

    private File resolveGitDir(File basedir) throws IOException {
        File gitdir = new RepositoryBuilder().findGitDir(basedir).getGitDir();
        if (gitdir == null) {
            throw new IOException("Could not resolve git repository for " + basedir);
        }
        // NOTE JGit does not resolve a worktree all the way to the main git repository
        File commondirFile = new File(gitdir, "commondir");
        if (commondirFile.exists()) {
            String relativePathToMainGitdir = Files.readString(commondirFile.toPath()).stripTrailing();
            gitdir = new File(gitdir, relativePathToMainGitdir).getCanonicalFile();
        }
        return gitdir;
    }
}
