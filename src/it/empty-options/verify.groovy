File nodeDir = new File(basedir, "build/node")
assert nodeDir.isDirectory()
File siteIndexFile = new File(basedir, "build/site/test/1.0/index.html")
assert siteIndexFile.isFile()
String stdout = new File(basedir, "build.log").text
assert stdout.contains("Running 'npx --yes --package antora antora --to-dir build${File.separator}site antora-playbook.yml'")
