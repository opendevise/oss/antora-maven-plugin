File nodeDir = new File(basedir, "target/node")
assert nodeDir.isDirectory()
File nodeModulesDir = new File(basedir, "node_modules")
assert !nodeModulesDir.exists()
File siteIndexFile = new File(basedir, "target/site/test/1.0/index.html")
assert siteIndexFile.isFile()
String indexText = siteIndexFile.text
assert indexText.contains(" data-api-key=\"the-algolia-api-key\"")
assert indexText.contains(" data-app-id=\"the-algolia-app-id\"")
assert indexText.contains(" data-index-name=\"the-algolia-index-name\"")
String stdout = new File(basedir, "build.log").text
assert stdout.contains("NODE_OPTIONS=--max-old-space-size=4096 --no-global-search-paths")
